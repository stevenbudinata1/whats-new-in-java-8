package introduction;

import java.io.File;
import java.io.FileFilter;

public class FirstLambda {
    public static void main(String[] args) {
        FileFilter fileFilter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".java");
            }
        };

        FileFilter fileFilterLambda = (File pathname) -> pathname.getName().endsWith(".java");

        File dir = new File("/tmp");
        File[] files = dir.listFiles(fileFilterLambda);

        for (File f : files) {
            System.out.println(f);
        }
    }
}
