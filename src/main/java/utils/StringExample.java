package utils;

import java.util.StringJoiner;
import java.util.stream.IntStream;

public class StringExample {
    public static void main(String[] args) {
        String s = "Hello world!";
        IntStream stream = s.chars();

        stream.mapToObj(letter -> (char)letter)
                .map(Character::toUpperCase)
                .forEach(System.out::print);

        StringJoiner sj = new StringJoiner(", ", "{", "}");
        sj.add("one").add("two").add("three");
        System.out.println(sj);

        String str = String.join(",", "one", "two", "three");
        System.out.println(str);

        String[] tab = {"one", "two", "three"};
        String str2 = String.join(",", tab);
        System.out.println(str2);
    }
}
