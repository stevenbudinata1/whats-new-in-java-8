package utils;

import utils.model.Person;

import java.util.*;

public class BuildingBiMap {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>(){{
            add(new Person("first", 10, "M"));
            add(new Person("second", 20, "F"));
            add(new Person("third", 5, "M"));
            add(new Person("four", 20, "F"));

            add(new Person("five", 10, "M"));
            add(new Person("six", 11, "F"));
            add(new Person("seven", 15, "M"));
            add(new Person("eight", 5, "M"));
        }};

        Map<Integer, Map<String, List<Person>>> bimap = new HashMap<>();

        persons.forEach(
                person -> bimap.computeIfAbsent(
                        person.getAge(),
                        HashMap::new
                ).merge(
                        person.getGender(),
                        new ArrayList<>(Arrays.asList(person)),
                        (l1, l2) -> {
                            l1.addAll(l2);
                            return l1;
                        }
                )
        );

        bimap.forEach(
                (age, m) -> System.out.println(age + " -> " + m)
        );
    }
}
