package utils;

import utils.model.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MergingMaps {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>(){{
            add(new Person("first", 10));
            add(new Person("second", 20));
            add(new Person("third", 5));
            add(new Person("four", 20));

            add(new Person("five", 10));
            add(new Person("six", 11));
            add(new Person("seven", 15));
            add(new Person("eight", 5));
        }};

        Map<Integer, List<Person>> map1 = mapByAge(persons.subList(0, 3));
        map1.forEach((age, list) -> System.out.println(age + " -> " + list));

        System.out.println("===");
        Map<Integer, List<Person>> map2 = mapByAge(persons.subList(4, 7));
        map2.forEach((age, list) -> System.out.println(age + " -> " + list));

        map2.entrySet().stream()
                .forEach(
                        entry -> map1.merge(
                                entry.getKey(),
                                entry.getValue(),
                                (l1, l2) -> {
                                    l1.addAll(l2);
                                    return l1;
                                }
                        )
                );

        System.out.println("Merged");
        map1.forEach((age, list) -> System.out.println(age + " -> " + list));
    }

    private static Map<Integer, List<Person>> mapByAge(List<Person> list) {
        Map<Integer, List<Person>> map = list.stream().collect(
                Collectors.groupingBy(
                        Person::getAge
                )
        );

        return map;
    }
}
