package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileReaderExample {
    public static void main(String[] args) {
        Path path = Paths.get(".", "example.txt");
        try (Stream<String> stream = Files.lines(path)) { // there's also Files.list to explore directories or Files.walk to explore subdirectories too
            stream.filter(line -> line.contains("one"))
                    .findFirst()
                    .ifPresent(System.out::println);
        } catch (IOException ioe) {
            // do something here
        }
    }
}
