package javafxexample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class FirstApplication extends Application {
    public static void main(String[] args) {
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Label label = new Label("Hello world!");
        Scene scene = new Scene(label);
        primaryStage.setScene(scene);
    }
}
