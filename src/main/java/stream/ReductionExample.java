package stream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ReductionExample {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(10, 10, 10);

        Integer ans1 = list.stream()
                        .reduce(0, (i1, i2) -> i1 + i2);
        System.out.println(ans1);

        list = Arrays.asList(10);
        Integer ans2 = list.stream()
                        .reduce(1, (i1, i2) -> i1 + i2);
        System.out.println(ans2); // if only one element in the stream, ans should be identity + the element

        list = Arrays.asList(-10, -10);
        Integer ans3 = list.stream()
                        .reduce(0, Integer::max);
        System.out.println(ans3); // it will return the wrong answer which is the identity

        list = Arrays.asList(-10, -10);
        Optional<Integer> ans4 = list.stream()
                .reduce(Integer::max);
        System.out.println(ans4.get()); // it will return the wrong answer which is the identity
    }
}
