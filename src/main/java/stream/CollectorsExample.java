package stream;

import stream.model.Person;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectorsExample {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>(){{
            add(new Person("first", 10));
            add(new Person("second", 20));
            add(new Person("third", 5));
            add(new Person("four", 20));
        }};
        Stream<Person> stream = persons.stream();

        Optional<Person> optionalPerson = stream.max(Comparator.comparing(Person::getAge));
        System.out.println(optionalPerson);

        Map<Integer, List<Person>> map = persons.stream()
                .collect(
                        Collectors.groupingBy(
                                Person::getAge
                        )
                );
        System.out.println(map);

        Map<Integer, Long> map2 = persons.stream()
                .collect(
                        Collectors.groupingBy(
                                Person::getAge,
                                Collectors.counting()
                        )
                );
        System.out.println(map2);

        Map<Integer, List<String>> map3 = persons.stream()
                .collect(
                        Collectors.groupingBy(
                                Person::getAge,
                                Collectors.mapping(
                                        Person::getName,
                                        Collectors.toList() // we can also use TreeSet / joining by ","
                                )
                        )
                );
        System.out.println(map3);
    }
}
