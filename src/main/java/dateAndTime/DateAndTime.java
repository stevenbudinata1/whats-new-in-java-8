package dateAndTime;


import dateAndTime.model.Person;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class DateAndTime {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>(){{
            add(new Person("first", LocalDate.of(1999, Month.APRIL, 1)));
            add(new Person("second", LocalDate.of(1957, Month.FEBRUARY, 10)));
            add(new Person("third", LocalDate.of(1945, Month.AUGUST, 17)));
            add(new Person("four", LocalDate.of(2000, Month.DECEMBER, 25)));
        }};

        LocalDate now = LocalDate.now();
        persons.stream()
                .forEach(p -> {
                    Period period = Period.between(p.getDateOfBirth(), now);
                    System.out.println(p.getName() + " was born " + period.get(ChronoUnit.YEARS) + " years "
                        + period.get(ChronoUnit.MONTHS) + " month"); // this only show the number of months after the year

                    System.out.println(p.getDateOfBirth().until(now, ChronoUnit.MONTHS));
                });
    }
}
