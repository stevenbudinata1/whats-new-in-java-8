# Whats new in java 8
This repo contains files from Pluralsight: What's new in Java 8 course (https://app.pluralsight.com/library/courses/java-8-whats-new).

I took some notes during the course which can be found here: https://drive.google.com/file/d/1gWsSfYsbLw-iCr6ItA8BtpNoWp__TqzX/view?usp=sharing
